GDBPM
=====

This project is a Python package that aims to provide useful features inside
GDB. In order to achieve this, it uses GDB’s Python API.


Usage
-----

To use it, first make sure that the gdbpm directory (the one that is inside
this repository) is in Python’s search path.  This can be done by just
appending it to Python’s "sys.path".

Then, import and instanciate the commands you want to use. Most modules in the
gdbpm package contain a command class, and instanciating with the command name
as the first argument is often enough to make it usable.


Example
-------

Here is a configuration example: first, in /home/user/.gdbinit:

    source /home/user/.gdbinit.py

And then in /home/user/.gdbinit.py:

    :::python
    # Add the /home/user/src directory to the module search path, so that
    # Python can find the /home/user/src/gdpbm package.
    import sys
    sys.path.append('/home/user/src/')

    # Create a "btc" command to display colored backtraces.
    import gdbpm.colored_backtrace
    gdbpm.colored_backtrace.ColoredBacktrace('btc')

    # Create a "disc" command to display colored disassemblies using Pygments'
    # terminal256 formatter.
    import gdbpm.colored_disas
    gdbpm.colored_disas.ColoredDisas('disc', 'terminal256', style='native')

    # Create a "pjson" command to display colored JSON dumps from the
    # inferior's dump_json(struct graph *g) function.
    import gdbpm.capture
    import gdbpm.pygmentized
    STDOUT = 1
    class PJSON(gdbpm.pygmentized.PygmentizedCommand):
        name = 'pjson'
        format_command = (lambda self, arg: 'call dump_json({0})'.format(arg))
        get_capturer = (lambda self, arg:
            gdbpm.capture.CaptureFD(STDOUT, copy=False))

        def __init__(self):
            super(PJSON, self).__init__(
                'json', {},
                'terminal256', {'style': 'native'},
                gdb.COMMAND_DATA, gdb.COMPLETE_SYMBOL
            )
    PJSON()
