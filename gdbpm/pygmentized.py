import gdb

from pygments import highlight

from gdbpm.capture import CapturingCommand
from gdbpm.utils import (
    catch_exceptions,
    get_lexer, get_formatter
)

class PygmentizedCommand(CapturingCommand):
    """Wrapper around another command that colorize its output."""

    def __init__(self,
        lexer, lexer_opts,
        formatter, formatter_opts,
        *args, **kwargs
    ):
        """Instanciate a colorizer wrapping command.

        lexer and lexer_opts are passed to gdbpm.utils.get_lexer to get a
        pygments lexer, and so are formatter and formatter_opts, for a pygments
        formatter.

        Other arguments are passed to the constructor of gdb.Command.
        """
        self.lexer = get_lexer(lexer, **(lexer_opts or {}))
        self.formatter = get_formatter(formatter, **(formatter_opts or {}))
        super(PygmentizedCommand, self).__init__(*args, **kwargs)

    @catch_exceptions
    def invoke(self, arg, from_tty):
        text = self.execute_and_capture(arg, from_tty)
        gdb.write(highlight(text, self.lexer, self.formatter))
