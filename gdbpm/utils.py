import traceback

import gdb

from pygments.formatters import get_formatter_by_name
from pygments.lexers import get_lexer_by_name


def catch_exceptions(func):
    """
    Return a wrapper around func that catches leaking exceptions, and print a
    nice Python backtrace when they occur.
    """

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except BaseException as exc:
            gdb.write(
                'An exception occured while executing the previous command:')
            gdb.write(traceback.format_exc())
            raise

    return wrapper


def iter_frames(start=None):
    """
    Iterate through the frames, starting at "start" or the selected frame.
    """
    frame = start or gdb.selected_frame()
    while frame:
        yield frame
        frame = frame.older()

def iter_blocks(frame, no_nested=True):
    """
    Iterate through the blocks related to the given frame.

    If no_nested, stop when meeting a block related to a nested function.
    """

    try:
        b = frame.block()
    except RuntimeError:
        # Probably: "cannot locate object file for block". Can happen when
        # there is no debug information.
        raise StopIteration()

    this_func = frame.function().linkage_name
    while b:
        if (
            no_nested and
            b.function is not None and
            b.function.linkage_name != this_func
        ):
            break
        yield b
        b = b.superblock

def get_lexer(lexer, **lexer_opts):
    """Return a lexer.

    If lexer is a string, look for the corresponding lexer and  instanciate it
    with the given options. Otherwise, return lexer as-is.
    """
    return (
        get_lexer_by_name(lexer, **lexer_opts)
        if isinstance(lexer, str) else
        lexer
    )

class Language(object):
    """Utility to temporarily switch to another language in GDB."""

    def __init__(self, name):
        self.name = name or 'auto'
        self.old_lang = None

    def set_lang(self, name):
        gdb.execute(
            'set lang {0}'.format(name),
            from_tty=False, to_string=True
        )

    def __enter__(self):
        self.old_lang = gdb.parameter('lang')
        self.set_lang(self.name)

    def __exit__(self, type, value, traceback):
        self.set_lang(self.old_lang)

def get_formatter(formatter, **formatter_opts):
    """Return a formatter.

    Likewise, but work on formatters.
    """
    return (
        get_formatter_by_name(formatter, **formatter_opts)
        if isinstance(formatter, str) else
        formatter
    )
