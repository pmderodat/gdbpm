import gdb

from pygments import highlight

from gdbpm.capture import CapturingCommand
from gdbpm.search import search_value, TypeNotFound, ValueNotFound
from gdbpm.utils import (
    catch_exceptions,
    get_formatter, get_lexer,
    Language
)


class DumpCallCommand(CapturingCommand):
    """Wrapper around an inferior call that colorize its output or dump it to
    some file.
    Usage: <cmdname> [>/dump/file] [expr]"""

    name = None
    func = None

    lexer = None
    formatter = None

    value_name = None
    value_type = None

    capturer = None

    def __init__(self):
        super(DumpCallCommand, self).__init__(
            gdb.COMMAND_DATA, gdb.COMPLETE_SYMBOL
        )

    def parse_arg(self, arg):
        filename = None
        value = None

        args = arg.split(None, 1)
        if args and args[0].startswith('>'):
            filename = args[0][1:]
            arg = args[1] if len(args) == 2 else ''

        if arg.strip():
            value = gdb.parse_and_eval(arg)
        else:
            # The value to dump is not provided: look for it in the stack
            # frames.
            value = search_value(self.value_name, self.value_type)

        value = value.cast(gdb.lookup_type('void').pointer())

        return (filename, value)

    def get_capturer(self, arg):
        return self.capturer

    def format_command(self, arg):
        return 'call {0}({1})'.format(self.func, self.invoked_value)

    @catch_exceptions
    def invoke(self, arg, from_tty):
        try:
            self.invoked_filename, self.invoked_value = self.parse_arg(arg)
        except (TypeNotFound, ValueNotFound, gdb.error) as e:
            gdb.write(str(e))
            gdb.write('\n')
            return

        with Language(self.language):
            dump = self.execute_and_capture(self.invoked_value, from_tty)

        if self.invoked_filename:
            # If dumping to a filename, write the dump as-is.
            with open(self.invoked_filename, 'w') as f:
                f.write(dump)
        else:
            # Otherwise, colorize it.  Assume the command output contains only
            # ASCII and ignore errors: they can come from typos, a Python error
            # is disturbing and not useful for users.
            gdb.write(highlight(
                dump.decode('ascii', errors='ignore'),
                get_lexer(self.lexer),
                get_formatter(self.formatter)).encode('ascii', errors='ignore'))
