"""
Define a few utilities to ease the handling of colors in terminal output.
"""

from collections import namedtuple

CLEAR   = '0'
BOLD    = '1'

Color = namedtuple('Color', 'number')

BLACK   = Color('0')
RED     = Color('1')
GREEN   = Color('2')
YELLOW  = Color('3')
BLUE    = Color('4')
MAGENTA = Color('5')
CYAN    = Color('6')
WHITE   = Color('7')

# Simple chuck of text with a single style.
StyledFragment = namedtuple('StyledFragment', 'style string')

class FragmentList(object):
    """
    List of fragments, each having its own style.
    """

    def __init__(self, fragments=[]):
        self.fragments = fragments

    def __str__(self):
        # TODO: make the formating somewhat more clever (removing unecessary
        # style resets, for instance).
        return ''.join(
            '\x1b[{0}m{1}\x1b[0m'.format(frg.style, frg.string)
            for frg in self.fragments
        )

class Style(object):
    """
    Factory for StyledString defaulting to a specific style.
    """

    def __init__(self, *fg_styles, **kwargs):
        styles = list(
            ('3' + style.number) if isinstance(style, Color) else style
            for style in fg_styles
        )
        try:
            bg = kwargs.pop('bg')
        except KeyError:
            pass
        else:
            assert isinstance(bg, Color)
            styles.append('4' + bg.number)

        if kwargs:
            raise TypeError('Invalid arguments: {}'.format(
                ', '.join(kwargs.keys())
            ))

        self.style = ';'.join(styles)

    def __call__(self, *fragments):
        """
        Return a fragment list created from input.
        
        Each item can be a fragment list itself or a mere string. These will be
        converted to fragments with the style defined by this instance.
        """
        converted = []
        for frg in fragments:
            if isinstance(frg, FragmentList):
                frg = frg.fragments
            else:
                frg = [StyledFragment(self.style, str(frg))]
            converted.append(frg)

        return FragmentList(sum(converted, []))

default = Style()

def colored(*fragments):
    return str(default(*fragments))

if __name__ == '__main__':
    red = Style(RED)
    green_ = Style(GREEN, BOLD)
    white_on_blue = Style(bg=BLUE)
    print(default(
        white_on_blue('(prompt)'),
        ' ',
        green_('print'),
        ' ',
        'func', red('('), 'arg', red(')')
    ))
