import gdb

from pygments import format
from pygments.token import *

from gdbpm.colors import (
    BOLD, GREEN, YELLOW, BLUE, CYAN, WHITE,
    Style, colored
)
from gdbpm.utils import (
    catch_exceptions,
    get_formatter,
    iter_blocks,
    iter_frames,
)

default = Style(WHITE)
frame_no = Style(BOLD, YELLOW)
frame_pc = Style(BLUE)
frame_func = Style(CYAN)
frame_file = Style(GREEN)
frame_line = Style()

func_arg_name = Style()
func_arg_value = default

class ColoredBacktrace(gdb.Command):
    """
    Print a backtrace like the one with "backtrace" or "info stack", but with a
    colored output.
    Usage: no argument
    """

    def __init__(
        self, name='btc',
        formatter='terminal',
        **formatter_opts
    ):
        super(ColoredBacktrace, self).__init__(
            name, gdb.COMMAND_STACK, gdb.COMPLETE_NONE)

        self.formatter = get_formatter(formatter, **formatter_opts)

    @catch_exceptions
    def invoke(self, arg, from_tty):
        result = []

        try:
            frame = gdb.newest_frame()
        except gdb.error:
            gdb.write('No stack.\n')
            return

        for i, frame in enumerate(iter_frames(gdb.newest_frame())):
            func = frame.function()
            result.extend([
                (Punctuation, '#'),
                (Number.Integer, str(i).ljust(2)),
                (Text, ' '),
                (Number.Hex, '{0:#0x}'.format(frame.pc())),
                (Text, ' in '),
                (Name.Function, frame.name() or '??'),
                (Text, ' '), (Punctuation, '('),
            ])

            # Collect arguments in the various blocks.
            args = []
            for block in iter_blocks(frame):
                for sym in block:
                    if sym.is_argument:
                        args.append(sym)

            # And then add them to the output, the name along with the
            # corresponding value if it's not too long.
            for arg_no, arg in enumerate(args):
                try:
                    value = str(arg.value(frame))
                except AttributeError:
                    value = (Generic.Error, '???')
                else:
                    if len(value) > 10:
                        value = (Punctuation, '...')
                    else:
                        value = (Literal, str(value))

                if arg_no > 0:
                    result.extend([(Punctuation, ','), (Text, ' ')])
                result.extend([
                    (Name.Variable, arg.print_name),
                    (Operator, '='),
                    value,
                ])
            result.extend([(Punctuation, ')'), (Text, '\n')])

            if func:
                result.extend([
                    (Text, '    at '),
                    (String.Other, func.symtab.filename),
                    (Text, ':'),
                    (Number.Integer, str(frame.find_sal().line)),
                    (Text, '\n'),
                ])

        gdb.write(format(result, self.formatter))
