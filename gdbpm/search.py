import gdb

from gdbpm.utils import iter_frames, iter_blocks, Language


class TypeNotFound(Exception):
    def __init__(self, name):
        super(TypeNotFound, self).__init__(name)

    def __str__(self):
        return 'Could not find any type called {0}'.format(self.args[0])

class ValueNotFound(Exception):
    pass


def search_value(name=None, type=None, language=None):
    """Look for the nearest NAME variable with type TYPE in stack frames."""

    if type:
        with Language(language):
            try:
                type = gdb.lookup_type(type)
            except gdb.error:
                raise TypeNotFound(str(type))

    def search_in_frame(frame):
        if name:
            try:
                values = [frame.read_var(name)]
            except ValueError:
                values = []
        else:
            values = sum((list(block) for block in iter_blocks(frame)), [])

        for value in values:
            if type is None or value.type == type:
                return value

    try:
        selected_frame = gdb.selected_frame()
    except gdb.error:
        raise ValueNotFound('Could not find any value: no program is running')

    # First look in callers' frames.
    for frame in iter_frames(selected_frame):
        value = search_in_frame(frame)
        if value:
            return value
    # Fallback to callees' frames.
    for frame in iter_frames(gdb.newest_frame()):
        if frame == gdb.selected_frame():
            break
        value = search_in_frame(frame)
        if value:
            return value

    raise ValueNotFound('Could not find any values{}{}'.format(
        ' called {}'.format(name) if name else '',
        ' whose type is {}'.format(type) if type else ''
    ))
