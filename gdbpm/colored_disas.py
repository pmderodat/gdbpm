import gdb

from pygments.lexer import RegexLexer, bygroups
from pygments.token import *

from gdbpm.pygmentized import PygmentizedCommand

class GDBDisasLexer(RegexLexer):
    """Pygments lexer for the GDB disassemble command."""

    string = r'"(\\"|[^"])*"'
    char = r'[a-zA-Z$._0-9@-]'
    identifier = r'(?:[a-zA-Z$_]' + char + '*|\.' + char + '+)'
    number = r'(?:0[xX][a-zA-Z0-9]+|\d+)'

    tokens = {
        'root': [
            (r'(   |=> )('+number+')(\s*)'
             r'(<)('+identifier+')?(\+)('+number+')(>:)(\s*)',
                bygroups(
                    Text, Number.Hex, Text,
                    Punctuation, Name.Function, Punctuation,
                    Number.Integer, Punctuation, Text
                ),
                'insn'),
            (r'.+$', Text),
        ],

        'insn': [
            (r'lock|rep(n?z)?|data\d+', Name.Attribute),
            (identifier, Name.Function, 'args'),

            (r'(#|;).*$', Comment.Single, '#pop'),
            (r'[\n\r]+', Text, '#pop'),
            (r'\s+', Text),
        ],

        'args': [
            (r'[,()]', Punctuation),
            (r'('+number+')( )(<)('+identifier+')(([-+])('+number+'))?(>)',
                bygroups(
                    Number, Text, Punctuation, Name.Constant,
                    None, Punctuation, Number, Punctuation)),
            (r'(\*)?(%'+identifier+')', bygroups(Operator, Name.Variable)),
            (r'(\*)?(-?\$?'+number+')', bygroups(Operator, Number)),

            (r'(#|;).*$', Comment.Single, '#pop:2'),
            (r'\s*[\r\n]+', Text, '#pop:2'),
            (r'\s+', Text),
        ]
    }

class ColoredDisas(PygmentizedCommand):
    """
    Print a disassembly like the one with "disas", but with a colored output.
    Usage: see "help disas".
    """

    def __init__(self, name, formatter='terminal', **formatter_opts):
        self.name = name

        super(ColoredDisas, self).__init__(
            GDBDisasLexer(), None,
            formatter, formatter_opts,
            gdb.COMMAND_DATA, gdb.COMPLETE_SYMBOL
        )

    def format_command(self, arg):
        return 'disas {0}'.format(arg)

    def get_capturer(self, arg):
        return None

if __name__ == '__main__':
    import sys
    from pygments import highlight
    from pygments.formatters import get_formatter_by_name

    highlight(
        sys.stdin.read(),
        GDBDisasLexer(),
        get_formatter_by_name('terminal256', style='native'),
        sys.stdout
    )
