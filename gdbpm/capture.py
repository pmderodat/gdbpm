#! /usr/bin/env python

import os
import re
import sys
import tempfile

import gdb

from gdbpm.utils import Language


CALL_RESULT = re.compile(r'\$(?P<id>\d+) = (?P<value>.*)\n$', re.MULTILINE)

def inf_call(expr, value):
    result = gdb.execute('call ' + expr, from_tty=False, to_string=True)
    m = CALL_RESULT.match(result)
    if m:
        return value(m.group('value'))
    else:
        raise RuntimeError(
            'Could not get call return value: {0}'.format(result)
        )

def inf_open(filename, mode):
    fd = inf_call('open("{0}", {1})'.format(filename, mode), int)
    if fd < 0:
        raise RuntimeError('Inferior: could not open {0}'.format(filename))
    return fd

def inf_close(fd):
    status = inf_call('close({0})'.format(fd), int)
    if status < 0:
        raise RuntimeError('Inferior: could not close FD {0}'.format(fd))

def inf_dup(fd):
    dup_fd = inf_call('dup({0})'.format(fd), int)
    if dup_fd < 0:
        raise RuntimeError('Inferior: could not duplicate FD {0}'.format(fd))
    return dup_fd

def inf_dup2(src, dst):
    status = inf_call('dup2({0}, {1})'.format(src, dst), int)
    if status < 0:
        raise RuntimeError(
            'Inferior: could not duplicate FD {0} to {1}'.format(src, dst)
        )

class CaptureFD(object):
    """Utility to capture what is written to some file descriptor.

    This only works on x86_64 architectures.
    """

    def __init__(self, fd, copy=True):
        """Create a capturing session for inferior's FD.

        If "copy" is False, what is captured is not actually written to the
        file descriptior.
        """
        # FD to capture
        self.fd = fd
        self.copy = copy

        # Capturing is done by replacing FD with a temporary file. "buffer_fd"
        # is the result of open(temp_file), and saving_fd is the file
        # descriptor used to keep FD's the original file open.
        self.buffer_file = None
        self.buffer_fd = -1
        self.saving_fd = -1

        self.buffer_file = None

    def read(self):
        """Read all the captured content."""
        return self.buffer_bytes

    def start_capture(self):
        """Start a capture session.

        Replace FD with a buffer file."""
        self.buffer_file = tempfile.NamedTemporaryFile(prefix='gdb-capture-')
        with Language('c'):
            self.buffer_fd = inf_open(
                self.buffer_file.name,
                os.O_WRONLY | os.O_TRUNC
            )
            self.saving_fd = inf_dup(self.fd)
            inf_dup2(self.buffer_fd, self.fd)

    def stop_capture(self):
        """Stop a capture session.

        Restore the original FD"""
        with Language('c'):
            inf_dup2(self.saving_fd, self.fd)
            inf_close(self.buffer_fd)
        with self.buffer_file as f:
            self.buffer_bytes = f.read()
        self.buffer_file = None
        if self.copy:
            gdb.write(self.buffer_bytes)

    def __enter__(self):
        self.start_capture()

    def __exit__(self, type, value, traceback):
        self.stop_capture()

class CapturingCommand(gdb.Command):
    """
    Base class for commands that need to capture the output of another one.
    """

    # Name for the command
    name = None

    def __init__(self, *args, **kwargs):
        super(CapturingCommand, self).__init__(self.name, *args, **kwargs)

    def get_capturer(self, arg):
        """Return the capturer to get some content.

        This must be implemented by users.

        "arg" is the argument that is passed to this wrapper. Return None if
        the content to colorize comes from the GDB command. Otherwise, use the
        result to capture the content to colorize. For instance, to colorize
        the output on the inferior's stdout, return an instance of
        gdbpm.capture.CaptureFD.
        """
        raise NotImplementedError()

    def format_command(self, arg):
        """Format the wrapped command to execute.

        "arg" is the argument passed to this wrapper. This must be implemented
        by users.
        """
        raise NotImplementedError()

    def execute_and_capture(self, arg, from_tty):
        """Execute the "arg" comand and return the captured output."""

        def helper():
            return gdb.execute(
                self.format_command(arg),
                from_tty=from_tty,
                to_string=True
            )

        capturer = self.get_capturer(arg)
        if capturer:
            with capturer:
                helper()
            return capturer.read()
        else:
            return helper()
